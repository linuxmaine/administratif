
Le vendredi 11 février 2022 à 18h

**[]Bilan de l’assemblée générale 2022[]**

**[]Présents []**                                 

Aubin Pierre,

Breheret Jérôme,

Chenon Pierre 

Coste Jérôme,

Parisot Jean-Michel 

Vallas Nordine

Vivet Stéphane


## Transferts des pouvoirs de vote pour le bureau 


| De                     | à                   |
| ---                    | ---                 |
| Jeanine CAUDAL         | VALLAS Nordine      |
| Jean-Pierre POMMERAIS  | VALLAS Nordine      |
| Gil LAIDET             | VALLAS Nordine      |
| Thierry GAYET          | VALLAS Nordine      |
| Fabien BATAILLE        | VALLAS Nordine      |
| Même Julien            | VALLAS Nordine      |
| dominique CAMPAGNA     | Jérôme COSTE        |
| Jean-claude VINSONNEAU | Gil LAIDET          |
| Jean-Marie VANHERPE    | Jean-Michel PARISOT |
| Bertrand PERRIER       | Laurent VANNIER     |

 7 votants

 8 pouvoirs transférés

 2 pouvoirs annulés                               

# Les bilans de l'année 2021

## Bilan d’activités

### Permanences

Nombre d’ouvertures du mercredi après-midi : environ 40.

Nombre d’ouvertures du samedi après-midi : environ 8. 

Les portes du Pôle Coluche ont été fermées pratiquement toute l’année ; l’accueil du public a été fait uniquement sur rdv et masqué avec un nombre limité de personnes (6) et pas en simultané ; le protocole renforcé signé avec la mairie est respecté.

### *Campagne de dons*

***[]Ordinateurs  distribués par Linuxmaine[]***

2 dons de portables cette année (Association Simone Veil) - peu de demandes.

Il reste quelques ordinateurs (15) vieillissants: les donner en premier ou les garder au musée ...

## Installations GNU/Linux au local

Les installations réalisées ont généré 5 nouvelles adhésions. 

## Site Internet de l’association

Le site est toujours hébergé au local, le projet de le remettre sur le serveur du centre social n’étant plus d’actualité puisque la ville du Mans a décidé de s’en séparer. Ceci nous a contraint aussi à déplacer début 2022 la base de gestion d’adhérents « Galette ».

- Une nouvelle rubrique « Formations » a été ouverte en début d’année 2021, suite logique des travaux engagés par les groupes « formation » sur l’année 2020. 

- Un nouvel onglet « Bibliothèque » a été ajouté pour consultation des ouvrages en prêt pour les membres de Linuxmaine (dont un legs important d’ouvrages de fond par feue Armelle Cudennec).

- De nouveaux articles ont été écrits et publiés sur le site (un article collectif sur « Le stockage des données », un article collectif « Comprendre l’informatique » et un nouveau compte-rendu de livre « L’internet des familles modestes »).

## Interventions extérieures

Participation au « Brico-solidaire » de CHANGE en Mai 2021. Peut être participerons-nous à celui de Mars 2022.

## Internet au Pôle Coluche

L’Association U.M.C.S., au rez-de-chaussée du pôle Coluche, qui s’occupait jusqu’à présent du règlement du loyer de la box Bouygues commune aux associations qui y sont raccordées, ne voulant plus assurer cette tâche, Linuxmaine qui n’assurait jusque là que le suivi de la partie technique a repris à sa charge l’abonnement et le paiement du loyer mensuel afin de conserver le partage de la ligne internet.

Par ailleurs le débit internet correspondant à l’abonnement souscrit jusqu’ici étant plutôt faible (8 à 20 Mega bits) et s’étant même dégradé en 2020/2021 (3 Mega bits) suite à un mauvais entretien des lignes « cuivre », n*otre fournisseur d’accès Bouygues a proposé un raccordement à la fibre (200 à 300 Mega bits/seconde) à de nouvelles conditions. Le bureau s’occupe actuellement de mettre en place les outils de contrôle et de distribution de l’accès à internet ainsi qu’un protocole de partage financier des frais occasionnés. Ceci amène de nouveaux frais mensuels de l’ordre de 6 à 7 euros.*

## Bilan financier

| Soldes des comptes       | Au 31/12/2019 | Au 31/12/2020 | Au 31/12/2021 | 
| ---                      | ---           | ---           | ---           |
| Livret bleu              | 700,52        | 700,52        | 707,68        |
| Compte courant           | 548,29        | 421,76        | 872,78        |
| Fond de caisse (liquide) | 23,04         | 92,2          | 66,14         |

Le loyer 2021 a été réduit de 2/3 par la ville du Mans (cause covid) soit 69,67 €.

Charges prévisionnelles 2022 : 209 € de  loyer ; 222 € d’assurance ; location domaines web : 23,98 € ; loyer internet 67.03 € : **total 522.01 €**

## Bilan des projets 2021/2022

Suite des formations en cours (GNU/Linux, logiciels libres)  : à compléter

## Projets possibles pour 2022

### re-créer la branche « développement/programmation » de LinuxMaine 

Comme je l'ai indiqué à Nordine, j'ai eu pas mal de problèmes avec mon travail qui est passé en liquidation et qui nous a valu de travailler beaucoup plus. Cela m'a pas mal désorganisé sur les ateliers que je voulais lancer. Néanmoins j'avais pas mal avancé et je souhaite continuer dans cette voie. Je pense lancer les premiers cours dès que je peux. (Thierry GAYET)

###  Relance des activités

   * bidouille
   * entraide informaticiens : ouverture d'un forum privé sur le site à l'attention des membres informaticiens pour permettre des échanges techniques
se pose la question de l'animation 

### Listes de diffusion

La liste de diffusion est peu utilisée:

rechercher une solution "Discourse" (chat avec possibilité de paramétrer en liste de diffusion, ...)

Propositions de créer 2 listes (ou plus)

Cette proposition vise à «récupérer» les membres qui se sont désinscrits de la liste de diffusion pour cause de messages trop nombreux

1 liste mensuelle pour faire un point sur les actions de linuxmaine. 

1 liste libre pour que les membres échangent sur différents sujets

la proposition que  la liste soit transformée en lettre mensuelle d'info

### forum
l'évolution du forum est laissée au futur bureau 

Proposition de créer un groupe Linuxmaine dans un chat existant: [https://chat.lescommuns.org/home](https://chat.lescommuns.org/home)

 les discussions y sont faciles (comme IRC) et moins consommatrices de ressources qu'une liste mail - mais beaucoup de trackers !!!

...ou héberger nous-mêmes un chat ?


### Permanence en ligne
Proposition de faire une permanence en ligne 1 fois par semaine en visio.

Certaines personnes intéressées par les logiciels libres ne peuvent pas se déplacer facilement au Mans. Une visio le jeudi soir de 20h à 22h permettrait de rentrer en contact avec ces personnes


### forum 
Proposition de créer un forum linuxmaine

### changement de local

une proposition pour un local plus grand
l'étude de la proposition est retenue

## Résultat de l'élection du Bureau 2022

| Fonction           | Candidat       | Votes pour | Votes blanc | Votes autre |
| ---                | ---            | ---        | ---         | ---         |
| Président          | VALLAS Nordine | 7+8        |             |             |
| Président adjoint  | GAYET Thierry  | 7+8        |             |             |
| Secrétaire         | COSTE Jérôme   | 7+8        |             |             |
| Secrétaire adjoint | CAUDAL Jeanine | 7+8        |             |             |
| Trésorier          | LAIDET Gil     | 7+8        |             |             |
| Trésorier adjoint  | POMMERAIS jp   | 7+8        |             |             |


modification des statuts pour une ag à distance

jérôme , la loi n'oblige pas à faire des ag en présentiel



18h55 fin de l'AG

                                   
