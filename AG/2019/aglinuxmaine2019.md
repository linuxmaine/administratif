

Préparation de la prochaine AG de janvier 2020

1-Appel:
Inscrits                                                                                    Présents                                                                  Excusé  

   * 
Collet Emmanuel
Coste Jérôme
Gery bernard
Hérault André
Jouanneau Laurent (confirmé pas de pique-nique)
Laidet Gil (procuration de vinsonneau jean-claude)
Même Julien (confirmé pas de pique-nique)
Parisot Jean-Michel (procuration Eric STAUCH ; Jérôme BREHERET)
Vallas Nordine
Vanherpe JM  (confirmé pas de pique-nique)
Vannier Laurent

Excusés : 
Bréheret Jérôme
Vinsonneau jc (confirmé pas de pique-nique)

Votants 10 + 3  procurations


2-Bilans


JB pas d'activité formelle tout a été informel.
une demande de réparation de la prise téléphonique dans la local «technique»

suite à des incivilités au pôle coluche, la médiathèque des Ronceray propose une journée itinérante dans le quartier pour que les habitants fassent connaissance avec les structures situées dans le quartier.
Une réunion est prévue le 22 janvier avec les associations du pôle coluche.
débat: 
    responsabilité de la mairie, il n'y a plus de gardien
    il y avait une porte ouverte annuelle quand le Centre Social des Quartiers Sud gérait le lieu
    NV: linuxmaine est situé dans des quartiers populaires et doit être en relation avec ses habitants .
    
    idée de ne pas restreindre l'approche du Libre au logiciel
    
    BG un repair café 4 fois par mois à sillé-le-guillaume; bernard, habitant de Sillé, prendra contact. il propose de susciter la curiosité et faire connaître le réseau de musique libre. 
    
    "remplacer windows", est une approche peut-être trop simple du Libre mais nous permet d'expliquer simplement ce que nous faisons; de plus nous précisons toujours ce que sont les valeurs du Libre .
    NV les membres de LXM sont invités à enrichir le site
    JM: utiliser facebook pour faire de "l'antifacebook"
 JMP: objet majoritairement utilisé: le smartphone et non l'ordinateur (référence au livre l'« Internet des pauvres »; quel auteur ?) ;
LV inciter les gens à s'intéresser plus globalement à la culture



2-1 Des permanences (mercredi-samedi)
ouvertures du mercredi : 42
ouvertures du samedi : 11
total 53
notes sur la fréquentation: plus de femmes que d'hommes, plus d'extérieurs au Mans que de manceaux, beaucoup d'associations ;
NV: utilité de dénombrer les visites (par rapport au nombre d'adhésion par exemple)

2-2 De la campagne de dons: 
    
environ 25 machines sur 40 distribuées
BG les logiciels libres s'inscrivent dans l'idée de réemploi dans le cadre des repaircafé
LV les personnes qui utilisent les LL semblent devenir plus autonomes
BG: beaucoup de demande de maintenance dans les cybercentres (pour du Windows ?), notamment des personnes âgées.


2-3 Des repair café:

participation de Linux Maine à 2 évènements

samedi 7 septembre Laigné en Belin salle des fêtes la Bellinoise
samedi 5 octobre changé centre François Rabelais

JM en a fait plus en 2018 plus d'interventions électroniques
NV: aussi beaucoup de demande de réparations de smartphones et tablettes
 -> de nouveau, élargir l'accès au libre
 Partager les dates/l'information sur les Repair'Café en Sarthe sur la liste de diffusion ;
 Avoir une réflexion sur l'orientation à prendre pendant les Repair'Café :

   *     apporter du matériel pour le dépannage ?
   *     don de ce matériel ?
   *     Faire une présentation de ce qu'est Linux et le Libre ?
2-4 Du groupe Install party: néant

GL aucune activité du groupe
JM évènement intéressant mais nécessite une très bonne communication et un fort réseau, ce que nous n'avons pas. Possible de promouvoir le système de "Parrain-Linux".
Un partenariat projection documentaire ou film suivie d'un débat et d'une install party avec le cybercentre de Marolles-les-braults ? Permet d'utiliser la communication des autres structures ; « La bataille du Libre », « Lol, une affaire sérieuse » (sorti le 06/2019) ;
BG film de Nina PALEY disponibles sur archives.org, au format Libre ;(titre du film ?)

Questions sur la communication, des dépliants et autres volants fait (mais où sont les archives de ces documents ? FTP de Thierry ?)

JMP utilisation de OVS (on va sortir) pour faire de communication

2-5 Du groupe Annuaire:
    néant
JM constituer un annuaire des structures locales et des gens qui font la promotion du Libre et/ou qui l'utilisent ;
JM reste motivé pour travailler sur ce sujet ;


2-6 Du groupe Diffusion d'articles : 
    
résultats mitigés; la recherche d'auteurs n'a pas fonctionné comme attendu, mais certains membres (4) ont commencé à écrire quelques articles pour le site internet. 


ouvertures du mercredi : 42
ouvertures du samedi : 11
total 53
JMV mon intention était de gérer tous les aspect du site internet , aprendre un CMS
et que le comité de rédaction serait une émanation du groupe.
JMP par où passer les discutions liste de diffusion ou PAD
GL un pad pour le groupe diffusion d'article 

2-7 Du groupe Site Internet:
    
le groupe de départ a bien fonctionné (3 sur 4) pour les phases de structuration et d'installation (machine + logiciels) ; la réalisation finale s'est terminée en solitaire. Présentation du site mise en place courant 2019 :
    \url{https://local.linuxmaine.org/spip/}
    NV : Tout reste ouvert ;

possibilité de s'autohéberger -> question du débit et de la sécurité mais faisable


2-8 Des interventions à l'extérieur: (2 adhésions a 50 euros) ...

Intervention à l'école de Roézé sur Sarthe


3-Bilan financier

3-1 Soldes des comptes:  
    
|              | 31/12/2018| 31/12/2019
livret bleu    |    695.31 | 695.31 
compte courant |    690.14 | 548.29  (apurement des arriérés de loyers 2015, 2016, 2017+2018 : moins 788 euros)

 Fond de caisse (liquide) :                             23.04
 Charge prévisionnelles 2020 :
     Loyer  :     206      Assurance :     environ 216    ...

JMP propose de réduire la partie livret bleu et utiliser l'argent pour la com de LXM

3-2 cotisations

| Montant    | 2018 | 2019 |
| :----------| ---: | ---: |
| à 10 euros |    3 |    6 |
| à 25 euros |   18 |   23 |
| à 50 euros |    0 |    2 |
| total      |  495 |  735 |

membres (galette)|  22    |     27 |

4-Projets 2020

  * Groupe 1 : résolution des problèmes liés à l'installation d'imprimantes sous GNU/Linux ;
  * Groupe 2 : proposition, organisation et mise en œuvre de formations (GNU/Linux, logiciels libres, matériels, tablettes, téléphones...) ;
    * formations/ateliers/initiations/découvertes/présentations/informations ;
    * JM sous forme de prestation?
    * GL c'est possible d'un point de vu légal sans payer d'impôts dans une certaine limite de montants
    * LJ c'est possible avec les adhésions
    * Plusieurs demandes lors des contacts par courriel
  * Contact avec une liste pour les élections communales du Mans au sujet d'un partenariat pour que Linuxmaine apporte son soutien au développement de la connaissance des logiciels Libres par les enfants des écoles primaires lors de TAP (Temps d'Activités Périscolaires) ;
  * Groupe 3 : étude et résolution des difficultés d'installation dues à l'UEFI (linux, double-boot)


5-Divers: 
    - critère d'éligibilité des membres du bureau (proposition: membre depuis au moins 1 an)
    - "Aidez-nous à promouvoir le Libre en participant au Libre en Fête et en 
       communiquant autour de l'initiative dans l'Agenda du Libre ! :-)"
       
6 - Élection du Bureau

Candidats :

Président
jérome B  
contre 0
abs 1
pour 12

Secrétaire
JMP 
contre 0
abs 0
pour 10+3

Trésorier
GL 
contre 0
abs 1
pour 9+3

Président suppléant :
LV
contre 0
abs 1
pour 9+3

Secrétaire suppléant :
NV
contre 0
abs 1
pour 9+3

Trésorier suppléant :
JCV 
contre 0
abs 0
pour 10+3

clôture 17h

