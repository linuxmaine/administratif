
Si vous voulez apporter des modifications au fichier aglinuxmaine2018.txt, ne le faites pas ici mais plutôt sur le PAD.

Le fichier aglinuxmaine2018.txt est synchronisé avec https://mensuel.framapad.org/p/aglinuxmaine2019 toutes les 5 minutes

Toutes les modifications du PAD apparaîtreront dans les Commits (https://framagit.org/linuxmaine/administratif/commits/master).

Vous pouvez, aussi, proroser vos modifications dans Framagit.
